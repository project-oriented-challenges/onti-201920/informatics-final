from heapq import heappop, heappush

# считываем данные
n, e = [int(i) for i in input().split()]
a = [int(i) for i in input().split()]
k = [int(i) for i in input().split()]
j = [1] * n

# текущее время, энергия и количество включений мозга
t, cur_e, brain_on_cnt = -1, e, 0
# очередь с приоритетом
heap = []

# предподсчёт
max_k = max(k)
sum_k = [0] * (max_k + 1)
diff = [0] * (max_k + 1)
for i in range(1, max_k + 1):
    sum_k[i] = sum_k[i - 1] + i
    diff[i] = diff[i - 1] + i * (i - 1)

for i in range(n):
    # входим в i-ю комнату
    t += 1
    
    # уничтожаем все предметы, что можем
    m = min(cur_e, a[i])
    t += m
    cur_e -= m
    a[i] -= m
    
    # заходим в мини-бар i-й комнаты столько раз, сколько нужно
    if a[i] > 0:
        # если энергия от энергетиков «не пропадёт»
        if (k[i] // 2 + 1) * ((k[i] + 1) // 2) <= e:
            # определяем, сколько раз Николай подойдёт к мини-бару в текущей комнате
            l, r = 0, k[i] + 1
            while l + 1 < r:
                m = (l + r) // 2
                if sum_k[m] * k[i] - diff[m] < a[i]:
                    l = m
                else:
                    r = m
            r = min(k[i], r)  # ровно r раз

            t += (k[i] * 2 - r + 1) * r // 2
            cur_e = sum_k[r] * k[i] - diff[r]
            j[i] = r + 1
            
            # снова уничтожаем всё, что можем
            m = min(cur_e, a[i])
            t += m
            cur_e -= m
            a[i] -= m
        
        # если энергия от энергетиков «пропадёт»
        else:
            # определяем, сколько первых подходов произойдут без потери энергии
            l, r = 0, (k[i] + 1) // 2
            while l + 1 < r:
                m = (l + r) // 2
                if m * (k[i] + 1 - m) > e:
                    r = m
                else:
                    l = m
            cl = l  # ровно cl подходов
            
            # определяем, сколько раз Николай подойдёт к мини-бару в текущей комнате
            l, r = 0, k[i] + 1
            while l + 1 < r:
                m = (l + r) // 2

                if m <= cl:
                    cur_sum = sum_k[m] * k[i] - diff[m]
                elif m <= cl + k[i] - 2 * cl:
                    cur_sum = sum_k[cl] * k[i] - diff[cl] + e * (m - cl)
                else:
                    cur_sum = (sum_k[cl] * k[i] - diff[cl]) * 2 + e * (k[i] - 2 * cl) \
                              - sum_k[k[i] - m] * k[i] + diff[k[i] - m]

                if cur_sum < a[i]:
                    l = m
                else:
                    r = m
            r = min(k[i], r)  # ровно r раз

            t += (k[i] * 2 - r + 1) * r // 2
            if r <= cl:
                cur_e = sum_k[r] * k[i] - diff[r]
            elif r <= cl + k[i] - 2 * cl:
                cur_e = sum_k[cl] * k[i] - diff[cl] + e * (r - cl)
            else:
                cur_e = (sum_k[cl] * k[i] - diff[cl]) * 2 + e * (k[i] - 2 * cl) \
                        - sum_k[k[i] - r] * k[i] + diff[k[i] - r]
            j[i] = r + 1
            
            # снова уничтожаем всё, что можем
            m = min(cur_e, a[i])
            t += m
            cur_e -= m
            a[i] -= m
    
    # идём в другие комнаты за энергией, если можно и нужно
    while a[i] > 0 and brain_on_cnt < 1000 and len(heap) > 0:
        brain_on_cnt += 1
        cur_e = -heap[0][0]
        id = -heappop(heap)[1]
        t += (i - id) * 2 + k[id] + 1 - j[id]
        j[id] += 1
        if j[id] <= k[id]:
            heappush(heap, [-min(e, j[id] * (k[id] + 1 - j[id])), -id])
        
        # снова уничтожаем всё, что можем
        m = min(cur_e, a[i])
        t += m
        cur_e -= m
        a[i] -= m
        
    
    # если в комнате остались предметы
    if a[i] > 0:
        print(-1)  # то Николай потерялся
        exit(0)
    
    # иначе осталось добавить в очередь с приоритетом
    # информацию о комнате, если в ней есть энергетики
    if j[i] <= k[i]:
        heappush(heap, [-min(e, j[i] * (k[i] + 1 - j[i])), -i])

# и вывод ответа в конце программы
print(t)
