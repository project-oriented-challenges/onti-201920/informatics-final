n, k = map(int, input().split())
graph = [[] for i in range(n)]
for i in range(n - 1):
    v, u = map(int, input().split())
    v -= 1
    u -= 1
    graph[v].append(u)
    graph[u].append(v)

deleted = [False] * n
used = [False] * n
cnt = [len(graph[i]) for i in range(n)]
len = [0] * n
q = [0] * n
ql, qr = 0, 0

for i in range(n):
    if cnt[i] < 2:
        used[i] = True
        q[qr] = i
        qr += 1

answer = 0
while ql < qr:
    v = q[ql]
    ql += 1
    max1, max2 = 0, 0
    for u in graph[v]:
        if len[u] > 0:
            if len[u] > max1:
                max1, max2 = len[u], max1
            elif len[u] > max2:
                max2 = len[u]
        elif not used[u]:
            cnt[u] -= 1
            if cnt[u] == 1:
                used[u] = True
                q[qr] = u
                qr += 1

    if max1 + 1 + max2 > k:
        answer += 1
        deleted[v] = True
    else:
        len[v] = max1 + 1

print(answer)
for i in range(n):
    if deleted[i]:
        print(i + 1, end=' ')
