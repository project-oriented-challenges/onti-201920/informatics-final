#include "testlib.h"

#include <bits/stdc++.h>

using namespace std;

int main(int argc, char * argv[]) {
  registerTestlibCmd(argc, argv);

  int n = inf.readInt(1, 100000, "n");
  inf.readSpace();
  int k = inf.readInt(1, 100000, "k");
  inf.readEoln();
  vector < vector < int > > edges(n);
  for (int i = 0; i < n - 1; i++) {
    int u = inf.readInt(1, n, "u") - 1;
    inf.readSpace();
    int v = inf.readInt(1, n, "v") - 1;
    inf.readEoln();
    edges[u].push_back(v);
    edges[v].push_back(u);
  }
  inf.readEof();

  long long ja = ans.readLong();
  ans.readEoln();
  vector < bool > used(n, 0);
  for (int i = 0; i < ja; i++) {
    int v = ans.readInt(1, n, "next v") - 1;
    if (used[v])
      quitf(_fail, "wrong sequence (double %d)", v + 1);
    used[v] = 1;
  }

  vector < int > cnt(n, 0), len(n, 0);
  queue < int > q;

  for (int i = 0; i < n; i++) {
    if (used[i]) continue;
    for (int j: edges[i]) {
      if (!used[j]) {
        cnt[i]++;
      }
    }
  }
  for (int i = 0; i < n; i++) {
    if (!used[i] && cnt[i] <= 1) {
      used[i] = true;
      q.push(i);
    }
  }

  int answer = 0;
  while (!q.empty()) {
    int v = q.front();
    q.pop();
    int max1 = 0, max2 = 0;
    for (int u: edges[v]) {
      if (len[u] > 0) {
        if (len[u] > max1) {
          max2 = max1;
          max1 = len[u];
        } else if (len[u] > max2) {
          max2 = len[u];
        }
      } else if (!used[u] && --cnt[u] == 1) {
        used[u] = true;
        q.push(u);
      }
    }
    answer = max(answer, max1 + 1 + max2);
    len[v] = max1 + 1;
  }

  if (answer > k)
    quitf(_fail, "wrong sequense (%d > %d)", answer, k);

  long long pa = ouf.readLong();
  ouf.readEoln();
  if (ja < pa)
    quitf(_wa, "wrong answer");
  fill(used.begin(), used.end(), 0);
  for (int i = 0; i < pa; i++) {
    int v = ouf.readInt(1, n, "next v") - 1;
    if (used[v])
      quitf(_wa, "wrong sequence (double %d)", v + 1);
    used[v] = 1;
  }

  fill(cnt.begin(), cnt.end(), 0);
  fill(len.begin(), len.end(), 0);

  for (int i = 0; i < n; i++) {
    if (used[i]) continue;
    for (int j: edges[i]) {
      if (!used[j]) {
        cnt[i]++;
      }
    }
  }
  for (int i = 0; i < n; i++) {
    if (!used[i] && cnt[i] <= 1) {
      used[i] = true;
      q.push(i);
    }
  }

  answer = 0;
  while (!q.empty()) {
    int v = q.front();
    q.pop();
    int max1 = 0, max2 = 0;
    for (int u: edges[v]) {
      if (len[u] > 0) {
        if (len[u] > max1) {
          max2 = max1;
          max1 = len[u];
        } else if (len[u] > max2) {
          max2 = len[u];
        }
      } else if (!used[u] && --cnt[u] == 1) {
        used[u] = true;
        q.push(u);
      }
    }
    answer = max(answer, max1 + 1 + max2);
    len[v] = max1 + 1;
  }

  if (answer > k)
    quitf(_wa, "wrong sequense (%d > %d)", answer, k);

  if (ja > pa)
    quitf(_fail, "participant has better answer");

  quitf(_ok, "%d numbers", n);

}