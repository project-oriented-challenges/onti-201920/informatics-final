#include <bits/stdc++.h>

#include "testlib.h"

#
define forn(i, n) for (int i = 0; i < int(n); i++)

  using namespace std;

long double
dist(long double x1, long double y1, long double x2, long double y2) {
  return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

int
main(int argc, char * argv[]) {
  setName("Interactor for 'Collective password' problem.");
  registerInteraction(argc, argv);

  long double x = inf.readDouble(), y = inf.readDouble();
  int max_queries = inf.readInt();

  int queries = 0;
  while (true) {
    queries++;

    string q = ouf.readToken();

    if (q != "!" && q != "?")
      quitf(_wa, "String '%s' from stdin is incorrect", q.c_str());

    if (q == "?" && queries > max_queries)
      quitf(_wa, "So mach queries");

    if (q == "!") {
      long double px = ouf.readDouble(-1e18, 1e18, "px"), py =
        ouf.readDouble(-1e18, 1e18, "py");
      long double pd = dist(x, y, px, py);

      if (pd > 1.0)
        quitf(_wa, "Wrong guest [%Lf ? %Lf, %Lf ? %Lf, %Lf]", px, x, py,
          y, pd);

      tout << fixed << setprecision(20) << queries << " " << px << " " <<
        py << " " << pd << endl;
      quitf(_ok, "%Lf %Lf is guessed (%Lf %Lf %Lf)", x, y, px, py, pd);
    }

    long double px = ouf.readDouble(-1e18, 1e18, "px"), py =
      ouf.readDouble(-1e18, 1e18, "py");
    cout << fixed << setprecision(20) << dist(x, y, px,
      py) << endl << flush;
  }
}