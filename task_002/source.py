from decimal import Decimal

maxDist = Decimal(2e9)
print('?', -maxDist, 0, flush=True)
distLeft = Decimal(input())

print('?', maxDist, 0, flush=True)
distRight = Decimal(input())

p = (distLeft + distRight + Decimal(2.0) * maxDist) / Decimal(2.0)
S = (p * (p - distLeft) * (p - distRight) * (p - Decimal(2.0) * maxDist)).sqrt()
h = S / maxDist
x = (distLeft * distLeft - h * h).sqrt() - maxDist

print('?', x, h, flush=True)
dist = Decimal(input())

print('!', x, (h if dist < Decimal(1.0) else -h), flush=True)
